module Lottery
  ( Lottery(..)
  , drawWinner
  ) where

import qualified Data.Map.Strict as M
import System.Random
import Data.List.Safe
import Prelude hiding ((!!))

data Lottery = Lottery
  { generator :: StdGen
  , tickets :: M.Map TicketHolder Amount
  }

type TicketHolder = String
type Winner = TicketHolder
type Amount = Int

drawWinner :: Lottery -> Maybe Winner
drawWinner l =
  let oneTicketholderPerTicket =
        M.foldrWithKey' (\t a ll -> ll ++ replicate a t) [] (tickets l)
      range = (0, max (length oneTicketholderPerTicket - 1) 0)
      randomIndex = randomR range (generator l)
  in
    oneTicketholderPerTicket !! fst randomIndex
