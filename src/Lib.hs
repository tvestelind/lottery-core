{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}

module Lib
    ( startApp
    , app
    ) where

import Network.Wai.Handler.Warp
import qualified Data.Map.Strict as M
import Servant
import Lottery
import System.Random
import Control.Monad.IO.Class

type LotteryDTO = M.Map String Int
type WinnerDTO = String

type API = "lottery" :> ReqBody '[JSON] LotteryDTO :> Post '[JSON] WinnerDTO

startApp :: IO ()
startApp = run 8080 app

app :: Application
app = serve (Proxy :: Proxy API) server

server :: Server API
server = winner

winner :: LotteryDTO -> Handler WinnerDTO
winner l = do
  gen <- liftIO newStdGen
  case drawWinner (Lottery gen l) of
    Just w  -> return w
    Nothing -> throwError (err409 { errReasonPhrase = "Couldn't draw a winner "})
