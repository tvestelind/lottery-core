FROM debian:10-slim
ARG BINARY_PATH

COPY $BINARY_PATH /opt/lottery-core

CMD /opt/lottery-core